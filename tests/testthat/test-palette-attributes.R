test_that("Testing attributes", {
  x <- abyayala.palettes(1)
  testthat::expect_equal(names(AbyaYalaPalettes)[1], attr(x, "name"))
  testthat::expect_s3_class(x, "palette")
  testthat::expect_vector(x, size = length(AbyaYalaPalettes[[1]][[1]]))
  testthat::expect_false(attr(x, "show.hex"))
})
