Paletas de colores AbyaYala
================

<!-- README.md is generated from README.Rmd. Please edit that file -->

![Al Galope 1970. Francisco Toledo. Imagen tomada de
<https://uploads1.wikiart.org/images/francisco-toledo/al-galope-1970.jpg>](man/figures/al-galope-1970.jpg)

> …Amarillo es el canario<br> Y Rosita mi abuelita<br> Amarillo es el
> canario<br> Y Rosita mi abuelita<br> Que daría la vida misma<br> Por
> tener otro color…

[*Colores*](https://www.youtube.com/watch?v=UJ6JDBjzbn0) por Ampersand

Esta librería está inspirada en la tradición del arte plástico
latinoamericano. Al construirla descubrí los colores de la tierra, el
amarillo del maíz, el azul de los mares increíbles del Abya Yala. Por
ahora incluye paletas derivada de obras de [Violeta
Parra](https://es.wikipedia.org/wiki/Violeta_Parra), [Aurora
Reyes](https://es.wikipedia.org/wiki/Aurora_Reyes), [Remedios
Varo](https://es.wikipedia.org/wiki/Remedios_Varo) y [Francisco
Toledo](https://es.wikipedia.org/wiki/Francisco_Toledo), [Beatriz
González](https://es.wikipedia.org/wiki/Beatriz_Gonz%C3%A1lez).

## Instalación

Actualmente la librería se encuentra disponible únicamente en este
repositorio, para instalarla puedes usar:

``` r
devtools::install_gitlab("rspatial_es/AbyaYala")
```

## Paletas

Existen diversos métodos para crear paletas de colores, en especial R
cuenta con varias librerías de colores y también es posible crear
paletas a partir de imágenes ([Paletas de
colores](https://myrbooksp.netlify.app/graph2.html)). En el caso de esta
librería se prefirió extraer los colores a partir de una imagen de la
obra usando [GPick](http://www.gpick.org). El código para la librería
fue tomado a partir de las librerías
[Wesanderson](https://github.com/karthik/wesanderson) y
[MetBrewer](https://github.com/BlakeRMills/MetBrewer/tree/main).
Actualmente la librería se compone de 18 paletas.

### Paletas inspiradas en la obra de Francisco Toledo

#### Al galope, 1970

<img src="https://uploads1.wikiart.org/images/francisco-toledo/al-galope-1970.jpg" width="47%" /><img src="man/figures/FT_al_galope-2.png" width="47%" />

#### Entrando en la noche, 1973

<img src="http://museoblaisten.com/images/coleccion/FT034.jpg" width="47%" /><img src="man/figures/FT_entrando_en_la_noche-2.png" width="47%" />

#### [Tamazul, 1977](https://museoblaisten.com/obra.php?id=3004&url=Tamazul)

<img src="http://museoblaisten.com/images/coleccion/FT025.jpg" width="47%" /><img src="man/figures/FT_Tamazul-2.png" width="47%" />

#### Autoretrato, 1990

<img src="https://uploads5.wikiart.org/images/francisco-toledo/self-portrait-1990.jpg" width="47%" /><img src="man/figures/FT_Autoretrato-2.png" width="47%" />

#### Diana, 1997

<img src="https://media.mutualart.com/Images/2011_11/13/10/102832410/291e4021-29a8-4796-876d-ddefe6b220d2.Jpeg" width="47%" /><img src="man/figures/FT_diana-2.png" width="47%" />

### Paletas inspiradas en la obra de Violeta Parra

#### Combate naval I

<img src="https://www.museovioletaparra.cl/wp-content/uploads/2021/07/Combate-naval-I-1345x179cm-Tela-de-arpillera-con-lanigrafia-1964-1965-_PNC9451-copia.jpg" width="47%" /><img src="man/figures/VP_combate_naval_i-2.png" width="47%" />

#### Combate naval II

<img src="https://www.museovioletaparra.cl/wp-content/uploads/2021/08/Combate-naval-II-132x220cm-Tela-de-arpillera-con-lanigrafia-1964-1965-_PNC9525-copia.jpg" width="47%" /><img src="man/figures/VP_combate_naval_ii-2.png" width="47%" />

#### La brujita

<img src="https://www.museovioletaparra.cl/wp-content/uploads/2021/08/PNC9779-1980x2552.jpg" width="47%" /><img src="man/figures/VP_la_brujita-2.png" width="47%" />

#### La Cueca

<img src="https://www.museovioletaparra.cl/wp-content/uploads/2020/09/La-cueca.jpg" width="47%" /><img src="man/figures/VP_la_cueca-2.png" width="47%" />

#### Los Conquistadores

<img src="https://www.museovioletaparra.cl/wp-content/uploads/2021/08/Los-conquistadores-142x196cm-Tela-de-arpillera-con-lanigrafia-1964-_PNC9409-copia.jpg" width="47%" /><img src="man/figures/VP_los_conquistadores-2.png" width="47%" />

### Paletas inspiradas en obras de Remedios Varo

#### Naturaleza muerta resucitando, 1963

<img src="https://i0.wp.com/www.remedios-varo.com/wp-content/uploads/2015/04/Cat.361-Naturaleza-Muerta-Resucitada-1963-1.jpg" width="47%" /><img src="man/figures/RV_Naturaleza_muerta_resucitando-2.png" width="47%" />

#### Personaje, 1961

<img src="https://i0.wp.com/www.remedios-varo.com/wp-content/uploads/2015/04/Cat.315-Personaje-1961-1.jpg" width="47%" /><img src="man/figures/RV_personaje-2.png" width="47%" />

#### Expedición del aqua áurea

<img src="https://i0.wp.com/www.remedios-varo.com/wp-content/uploads/2015/04/Cat.341-Expedicio%CC%81n-Del-Aqua-Aerea-1962-1.jpg" width="47%" /><img src="man/figures/RV_expedicion-2.png" width="47%" />

### Obras Inspiradas en obra de Aurora Reyes

#### Atentado a las maestras rurales, 1936

<img src="https://imagenes.razon.com.mx/files/image_940_470/uploads/2021/03/05/6042b8edc212a.jpeg" width="47%" /><img src="man/figures/AR_atentado-2.png" width="47%" />

#### El primer encuentro, 1978

<img src="https://cdn.local.mx/wp-content/uploads/2019/05/areyes_encuentro.webp" width="47%" /><img src="man/figures/AR_primer_encuentro-2.png" width="47%" />

### Obras Inspiradas en obra de Beatriz González

#### Los papagayos

<img src="https://uploads3.wikiart.org/00308/images/beatriz-gonzalez/papagayo-1.jpg" width="47%" /><img src="man/figures/papagayosWaffle-2.png" width="47%" />

#### Los suicidas

<img src="https://uploads6.wikiart.org/00308/images/beatriz-gonzalez/los-suicidas-del-sisga.jpg" width="47%" /><img src="man/figures/SuicidasWaffle-2.png" width="47%" />

#### Empalada

<img src="https://uploads7.wikiart.org/00308/images/beatriz-gonzalez/a9d9e30ea76e2a72391be648c2e74f44-1200x702.jpg" width="47%" /><img src="man/figures/EmpaladaWaffle-2.png" width="47%" />

## Modo de uso

Para llamar a la librería y ver las paletas disponibles puedes usar:

``` r
library("AbyaYala")

# Ver paletas
names(AbyaYalaPalettes)
#>  [1] "FT_al_galope"            "FT_entrando_en_la_noche"
#>  [3] "FT_tamazul"              "FT_autoretrato"         
#>  [5] "FT_diana"                "VP_combate_naval_i"     
#>  [7] "VP_combate_naval_ii"     "VP_brujita"             
#>  [9] "VP_cueca"                "VP_conquistadores"      
#> [11] "AR_atentado_maestras"    "AR_primer_encuentro"    
#> [13] "RV_naturaleza"           "RV_personaje"           
#> [15] "RV_expedicion"           "BG_empalada"            
#> [17] "BG_papagayo"             "BG_suicidas"
```

Para usar alguna de las paletas basta con

``` r
abyayala.palettes("FT_al_galope")
abyayala.palettes("VP_brujita")
abyayala.palettes("VP_cueca", 3)
abyayala.palettes("BG_empalada", n=8, type='continuous')
```

### Comentarios

El impulso de hacer la librería vino de la comunidad
[@RSpatial\_ES](https://t.me/rspatial_es) y del trabajo de quienes
crearon los paquetes
[wesanderson](https://github.com/karthik/wesanderson) y
[MetBrewer](https://github.com/BlakeRMills/MetBrewer), gracias por su
generosidad.
